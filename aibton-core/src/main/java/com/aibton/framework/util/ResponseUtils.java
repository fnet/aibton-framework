/**
 * Aibton.com Inc.
 * Copyright (c) 2004-2017 All Rights Reserved.
 */
package com.aibton.framework.util;

import com.aibton.framework.data.ResponseNormal;
import com.aibton.framework.data.ResponsePage;

/**
 * 返回页面工具类
 * @author huzhihui
 * @version v 0.1 2017/4/10 22:15 huzhihui Exp $$
 */
public class ResponseUtils {

    /**
     * 普通返回
     * @param success   请求成功与否状态
     * @param msg   数据
     * @param <T>   泛型类
     * @return  ResponseNormal
     */
    public static <T> ResponseNormal<T> getData(boolean success, T msg) {
        return ResponseNormal.getData(success, msg);
    }

    /**
     * 其他返回
     * @param success   请求成功与否状态
     * @param code  编码
     * @param msg   数据
     * @param <T>   泛型类
     * @return  ResponseNormal
     */
    public static <T> ResponseNormal<T> getOtherData(boolean success, String code, T msg) {
        return ResponseNormal.getData(success, code, msg);
    }

    /**
     * 分页返回参数
     * @param success   请求成功与否状态
     * @param currentPage   当前页
     * @param pageSize  一页大小
     * @param totalLine 总行数
     * @param totalPage 总页数
     * @param msg   列表数据
     * @param <T>   泛型类
     * @return  ResponsePage
     */
    public static <T> ResponsePage<T> getPageData(boolean success, Integer currentPage,
                                                  Integer pageSize, Integer totalLine,
                                                  Integer totalPage, T msg) {
        return ResponsePage.getData(success, currentPage, pageSize, totalLine, totalPage, msg);
    }
}
