/**
 * Aibton.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.aibton.framework.api.threadLocal;

import java.util.List;

import com.aibton.framework.api.auth.data.AuthData;

/**
 * Api 当前线程副本
 * @author huzhihui
 * @version $: v 0.1 2017 2017/7/24 15:58 huzhihui Exp $$
 */
public class ApiThreadLocalUtils {

    /**
     * 权限列表
     */
    private static ThreadLocal<List<AuthData>> authDatas = new ThreadLocal<>();

    /**
     * 获取权限列表
     * @return  权限列表值
     */
    public static List<AuthData> getAuthDatas() {
        return authDatas.get();
    }

    /**
     * 设置权限列表
     * @param authDatas 权限列表对象
     */
    public static void setAuthDatas(List<AuthData> authDatas) {
        ApiThreadLocalUtils.authDatas.set(authDatas);
    }
}
