/**
 * Aibton.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.aibton.framework.api;

import java.util.List;

import com.aibton.framework.api.Interceptor.IBaseApiInterceptor;
import com.aibton.framework.api.handel.IBaseApiHandel;
import com.aibton.framework.data.BaseRequest;
import com.aibton.framework.data.BaseResponse;

/**
 * API父类
 * @author huzhihui
 * @version $: v 0.1 2017 2017/7/20 15:44 huzhihui Exp $$
 */
public abstract class AbstractBaseApi<I extends BaseRequest, O extends BaseResponse> {

    /**
     *
     * API具体逻辑实现主入口
     * @param request 请求参数
     * @param response  返回参数
     * @return  执行结果
     */
    public abstract BaseResponse excute(I request, O response);

    /**
     * 父方法包装执行主入口
     * @param request   请求参数
     * @param response  返回参数
     * @return  返回值
     */
    public BaseResponse doExcute(I request, O response) {
        return excute(request, response);
    }

    /**
     * 获取用户自定义拦截器
     * @return  返回自定义拦截器列表
     */
    public List<IBaseApiInterceptor> getUserIBaseApiInterceptors() {
        return null;
    };

    /**
     * 获取用户自定义处理器
     * @return  返回自定义处理器列表
     */
    public List<IBaseApiHandel> getUserIBaseApiHandels() {
        return null;
    };

    /**
     * 获取用户自定义拦截器
     * @return  返回自定义拦截器列表
     */
    public List<IBaseApiInterceptor> getUserAfterIBaseApiInterceptors() {
        return null;
    };

    /**
     * 获取用户自定义处理器
     * @return  返回自定义处理器列表
     */
    public List<IBaseApiHandel> getUserAfterIBaseApiHandels() {
        return null;
    };
}
