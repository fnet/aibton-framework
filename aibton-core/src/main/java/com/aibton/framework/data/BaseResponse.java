/**
 * Aibton.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.aibton.framework.data;

import java.io.Serializable;

/**
 * 基本返回对象
 * @author huzhihui
 * @version $: v 0.1 2017 2017/7/20 15:35 huzhihui Exp $$
 */
public class BaseResponse implements Serializable {

}
