/**
 * Aibton.com Inc.
 * Copyright (c) 2004-2017 All Rights Reserved.
 */
package com.aibton.framework.data;

import java.io.Serializable;

/**
 * 基础请求bean
 * @author huzhihui
 * @version v 0.1 2017/5/11 21:57 huzhihui Exp $$
 */
public class BaseRequest implements Serializable {

}
